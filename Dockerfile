from alpine:3.8 as builder

run apk add autoconf
run apk add automake
run apk add expect
run apk add git
run apk add guile-dev
run apk add make
run apk add python

run git clone https://truesilver92@bitbucket.org/truesilver92/readable_lisp.git /src
workdir /src

run git checkout tags/1.0.10

run mkdir -p /build
run mkdir -p /install

workdir /src
run autoreconf -i

workdir /build
run /src/configure --prefix=/usr --without-common-lisp
run make
run make DESTDIR=/install install

from alpine:3.8

copy --from=builder /install/usr /usr

run apk add guile
run apk add expect

cmd /bin/sh
